import React, { useState, useRef } from "react";
import {
  SafeAreaView,
  StatusBar,
  View,
  TouchableOpacity,
  TextInput,
  Pressable,
  Keyboard,
  TouchableWithoutFeedback,
  ScrollView,
} from "react-native";
import { Header } from "../../components/basics/header";
import { CustomButton } from "../../components/basics/Button";
import { MaterialIcons, Entypo } from "@expo/vector-icons";
import { colors } from "../../styles/color";
import styles from "./ReceptionInfoEditStyles";
import { HiraginoKakuText } from "../../components/StyledText";
import { NavigationProp, useRoute } from "@react-navigation/native";
import { ReceptionInfoSaveDialog } from "../reception-info-save-dialog/ReceptionInfoSaveDialog";
import { format } from "date-fns";
import { CustomCalendar } from "../../components/basics/Calendar";

type Props = {
  navigation: NavigationProp<any, any>;
};
export const ReceptionInfoEdit = ({ navigation }: Props) => {
  const route = useRoute();
  const { userId, eventId, receptionId, registerType, pgType } =
    route.params as {
      userId: string;
      eventId: number;
      receptionId: number;
      registerType: string;
      pgType: string;
    };

  //1人で受付した場合 : useState(1)
  //const [acceptedPersons, setAcceptedPersons] = useState(1);
  const [acceptedPersons, setAcceptedPersons] = useState(3);
  const [selectedButton, setSelectedButton] = useState<string | null>(null);
  const [selectedOption, setSelectedOption] = useState("M");
  const [showDropdown, setShowDropdown] = useState(false);
  const [selectedDropDownValue, setSelectedDropDownValue] = useState("");
  const options = ["配偶者", "本人"];
  const [relationDropDownErr, setRelationDropDownErr] = useState(false);

  //InputBox
  const [firstName, setFirstName] = useState("出茂");
  const [lastName, setLastName] = useState("進次郎");
  const [firstNameKana, setFirstNameKana] = useState("出茂");
  const [lastNameKana, setLastNameKana] = useState("進次郎");
  const [birthDate, setBirthDate] = useState("2024-04-04");
  const [postCode, setPostCode] = useState("515-0004");
  const [address, setAddress] = useState(
    "三重県松阪市なんとか町101-1マンションなんとか103"
  );
  const [isBirthDateCalendarVisible, setBirthDateCalendarVisible] =
    useState(false);
  const birthDateInputRef = useRef(null);
  const birthDateRef = useRef(null);

  const handleButtonPress = (buttonName: string) => {
    if (buttonName === selectedButton) {
      setSelectedButton(null);
    } else {
      setSelectedButton(buttonName);
    }
    setBirthDateCalendarVisible(false);
  };

  const handleRadioSelectOption = (option: string) => {
    setSelectedOption((prevOption) =>
      prevOption === option ? prevOption : option
    );
  };

  const handleDropDownSelectOption = (option: string) => {
    setSelectedDropDownValue(option);
    setShowDropdown(false);
  };

  const handleCreateButtonPress = () => {
    if (!selectedDropDownValue.trim()) {
      setRelationDropDownErr(true);
    } else {
      setRelationDropDownErr(false);
    }
    navigation.navigate("ReceptionInfoDetail", {
      userId: 1,
      eventId: 1,
      receptionId: 1,
      pgType: pgType,
    });
  };

  //Reception Save Dialog
  const [isSaveModalVisible, setIsSaveModalVisible] = useState(false);
  const handleCloseButton = () => {
    setBirthDateCalendarVisible(false);
    setShowDropdown(false);
    setIsSaveModalVisible(true);
  };
  const handleCancelButton = () => {
    setIsSaveModalVisible(false);
    navigation.navigate("ReceptionInfoEdit", {
      userId,
      eventId: 1,
      receptionId: 1,
      registerType: registerType,
      pgType: pgType,
    });
  };
  const handleNotSaveButton = () => {
    setIsSaveModalVisible(false);

    navigation.navigate("ReceptionInfoDetail", {
      userId: userId,
      eventId: 1,
      receptionId: 1,
      pgType: pgType,
    });
  };
  const handleSaveButton = () => {
    setIsSaveModalVisible(false);
    navigation.navigate("ReceptionInfoDetail", {
      userId: userId,
      eventId: 1,
      receptionId: 1,
      pgType: pgType,
    });
  };

  const handleBirthDateCalendarPress = (event: any) => {
    (birthDateInputRef.current as any).focus();
    setBirthDateCalendarVisible(!isBirthDateCalendarVisible);
    setShowDropdown(false);
  };

  const handleBirthDateSelect = (date: any) => {
    setBirthDate(date);
    setBirthDateCalendarVisible(false);
  };

  const closeCalendar = (event: any) => {
    if (
      event.nativeEvent.target != birthDateInputRef.current &&
      event.nativeEvent.target != birthDateRef
    ) {
      if (isBirthDateCalendarVisible) {
        setBirthDateCalendarVisible(false);
      }
    }
  };

  return (
    <TouchableWithoutFeedback onPress={closeCalendar}>
      <SafeAreaView style={styles.mainContainer}>
        <StatusBar barStyle="dark-content" />
        <Header middleTitleName="受付情報編集" buttonName="">
          <CustomButton
            text="閉じる"
            type="ButtonSDefault"
            icon={<Entypo name="cross" size={24} color={colors.primary} />}
            iconPosition="front"
            onPress={handleCloseButton}
          />
        </Header>
        <ScrollView style={styles.container} scrollEnabled={true}>
          <View style={styles.bodyContainer}>
            {/* Conditionally render the left layout //acceptedPersons > 1 */}
            {registerType === "group" && (
              <View style={styles.leftContainer}>
                <View style={styles.leftTitleContainer}>
                  <HiraginoKakuText style={[styles.bodyText, styles.titleText]}>
                    受付人数
                  </HiraginoKakuText>
                  <HiraginoKakuText style={[styles.bodyText, styles.titleText]}>
                    {acceptedPersons}人
                  </HiraginoKakuText>
                </View>
                <View style={styles.leftButtonsContainer}>
                  <View style={styles.leftSubButtonContainer}>
                    <HiraginoKakuText
                      style={[styles.bodyText, styles.subTitleText]}
                    >
                      受付代表
                    </HiraginoKakuText>
                    <TouchableOpacity
                      style={[
                        styles.leftButton,
                        selectedButton === "btnMainPErson" &&
                          styles.btnSelected,
                      ]}
                      onPress={() => {
                        handleButtonPress("btnMainPErson");
                      }}
                    >
                      <HiraginoKakuText
                        style={[styles.bodyText]}
                        normal
                        numberOfLines={1}
                      >
                        出茂　太郎
                      </HiraginoKakuText>
                    </TouchableOpacity>
                  </View>
                  <View style={styles.leftSubButtonContainer}>
                    <HiraginoKakuText
                      style={[styles.bodyText, styles.subTitleText]}
                    >
                      一緒に受付
                    </HiraginoKakuText>
                    <TouchableOpacity
                      style={[
                        styles.leftButton,
                        selectedButton === "btnPerson1" && styles.btnSelected,
                      ]}
                      onPress={() => {
                        handleButtonPress("btnPerson1");
                      }}
                    >
                      <HiraginoKakuText
                        style={[styles.bodyText]}
                        normal
                        numberOfLines={1}
                      >
                        出茂　ふみ
                      </HiraginoKakuText>
                    </TouchableOpacity>
                    <TouchableOpacity
                      style={[
                        styles.leftButton,
                        selectedButton === "btnPerson2" && styles.btnSelected,
                      ]}
                      onPress={() => {
                        handleButtonPress("btnPerson2");
                      }}
                    >
                      <HiraginoKakuText
                        normal
                        style={[styles.bodyText]}
                        numberOfLines={1}
                      >
                        出茂　太郎あああああああああああああああああああああああああ
                      </HiraginoKakuText>
                    </TouchableOpacity>
                  </View>
                </View>
              </View>
            )}

            {/* Right layout */}
            <View style={styles.rightContainer}>
              <View style={styles.rightBodyContainer}>
                <View style={styles.nameContainer}>
                  <View style={styles.rightLabelContainer}>
                    <HiraginoKakuText
                      style={[styles.bodyText, styles.subTitleText]}
                    >
                      お名前
                    </HiraginoKakuText>
                  </View>
                  <View style={styles.nameInputBoxContainer}>
                    <TextInput
                      style={[styles.textInput, styles.nameInput]}
                      placeholder="出茂"
                      placeholderTextColor={colors.placeholderTextColor}
                      value={firstName}
                      onChangeText={setFirstName}
                      onFocus={() => setBirthDateCalendarVisible(false)}
                    />
                    <TextInput
                      style={[styles.textInput, styles.nameInput]}
                      placeholder="進次郎"
                      placeholderTextColor={colors.placeholderTextColor}
                      value={lastName}
                      onChangeText={setLastName}
                      onFocus={() => setBirthDateCalendarVisible(false)}
                    />
                  </View>
                </View>
                <View style={styles.nameContainer}>
                  <View style={styles.rightLabelContainer}>
                    <HiraginoKakuText
                      style={[styles.bodyText, styles.subTitleText]}
                    >
                      お名前（カナ）
                    </HiraginoKakuText>
                  </View>
                  <View style={styles.nameInputBoxContainer}>
                    <TextInput
                      style={[styles.textInput, styles.nameInput]}
                      placeholder="イズモ"
                      placeholderTextColor={colors.placeholderTextColor}
                      value={firstNameKana}
                      onChangeText={setFirstNameKana}
                      onFocus={() => setBirthDateCalendarVisible(false)}
                    />
                    <TextInput
                      style={[styles.textInput, styles.nameInput]}
                      placeholder="進次郎シンジロウ"
                      placeholderTextColor={colors.placeholderTextColor}
                      value={lastNameKana}
                      onChangeText={setLastNameKana}
                      onFocus={() => setBirthDateCalendarVisible(false)}
                    />
                  </View>
                </View>
                <View style={styles.datePickerContainer}>
                  <View style={styles.rightLabelContainer}>
                    <HiraginoKakuText
                      style={[styles.bodyText, styles.subTitleText]}
                    >
                      生年月日
                    </HiraginoKakuText>
                  </View>
                  <View style={styles.datePickerContainer}>
                    <TextInput
                      ref={birthDateInputRef}
                      style={styles.textInput}
                      placeholder="日付を選択"
                      placeholderTextColor={colors.placeholderTextColor}
                      value={
                        birthDate != ""
                          ? format(new Date(birthDate), "yyyy/MM/dd")
                          : birthDate
                      }
                      onPressIn={handleBirthDateCalendarPress}
                      onPointerDown={handleBirthDateCalendarPress}
                      showSoftInputOnFocus={false}
                      onTouchStart={() => Keyboard.dismiss()}
                      editable={false}
                    />
                    <Pressable
                      style={styles.calendarIconContainer}
                      ref={birthDateRef}
                      onPress={handleBirthDateCalendarPress}
                    >
                      <MaterialIcons
                        name="calendar-today"
                        size={22}
                        color={colors.activeCarouselColor}
                        style={styles.calendarIcon}
                      />
                    </Pressable>
                    {isBirthDateCalendarVisible && (
                      <CustomCalendar
                        selectedDate={birthDate}
                        onDateSelect={handleBirthDateSelect}
                      />
                    )}
                  </View>
                </View>
                <View style={styles.genderContainer}>
                  <View style={styles.rightLabelContainer}>
                    <HiraginoKakuText
                      style={[styles.bodyText, styles.subTitleText]}
                    >
                      性別
                    </HiraginoKakuText>
                  </View>
                  <View style={styles.genderRadioPanel}>
                    <Pressable
                      onPress={() => handleRadioSelectOption("M")}
                      style={[styles.radioBtnContainer]}
                    >
                      <View style={styles.radioButtonIcon}>
                        <RadioButton
                          selected={selectedOption === "M"}
                          style={styles.radioButton}
                        />
                      </View>
                      <HiraginoKakuText
                        normal
                        style={[styles.bodyText, styles.radioBtnText]}
                      >
                        男性
                      </HiraginoKakuText>
                    </Pressable>
                    <Pressable
                      onPress={() => handleRadioSelectOption("F")}
                      style={[styles.radioBtnContainer]}
                    >
                      <View style={styles.radioButtonIcon}>
                        <RadioButton
                          selected={selectedOption === "F"}
                          style={styles.radioButton}
                        />
                      </View>
                      <HiraginoKakuText
                        normal
                        style={[styles.bodyText, styles.radioBtnText]}
                      >
                        女性
                      </HiraginoKakuText>
                    </Pressable>
                    <Pressable
                      onPress={() => handleRadioSelectOption("N")}
                      style={[styles.radioBtnContainer, styles.radioNoAnsBox]}
                    >
                      <View style={styles.radioButtonIcon}>
                        <RadioButton
                          selected={selectedOption === "N"}
                          style={styles.radioButton}
                        />
                      </View>
                      <HiraginoKakuText
                        normal
                        style={[styles.bodyText, styles.radioBtnText]}
                      >
                        回答しない
                      </HiraginoKakuText>
                    </Pressable>
                  </View>
                </View>
                <View style={styles.postCodeContainer}>
                  <View style={styles.rightLabelContainer}>
                    <HiraginoKakuText
                      style={[styles.bodyText, styles.subTitleText]}
                    >
                      郵便番号
                    </HiraginoKakuText>
                  </View>
                  <View style={styles.postCodeBoxContainer}>
                    <TextInput
                      style={[styles.textInput, styles.postCodeInput]}
                      value={postCode}
                      onChangeText={setPostCode}
                      onFocus={() => setBirthDateCalendarVisible(false)}
                    />
                    <TouchableOpacity
                      style={[
                        styles.searchBtn,
                        selectedButton === "search" && styles.btnSelected,
                      ]}
                      onPress={() => handleButtonPress("search")}
                    >
                      <HiraginoKakuText
                        style={[styles.LabelLargeBold, styles.searchBtnText]}
                      >
                        住所検索
                      </HiraginoKakuText>
                    </TouchableOpacity>
                  </View>
                </View>
                <View style={styles.addressContainer}>
                  <View style={styles.rightLabelContainer}>
                    <HiraginoKakuText
                      style={[styles.bodyText, styles.subTitleText]}
                    >
                      住所
                    </HiraginoKakuText>
                  </View>
                  <TextInput
                    style={styles.textInput}
                    value={address}
                    onChangeText={setAddress}
                    onFocus={() => setBirthDateCalendarVisible(false)}
                  />
                </View>
                <View style={styles.relationshipContainer}>
                  <View style={styles.rightLabelContainer}>
                    <HiraginoKakuText
                      style={[styles.bodyText, styles.subTitleText]}
                    >
                      代表者との関係
                    </HiraginoKakuText>
                  </View>
                  <View style={styles.dropdownContainer}>
                    <TouchableOpacity
                      style={styles.textInput}
                      onPress={() => {
                        setShowDropdown(!showDropdown);
                        setBirthDateCalendarVisible(false);
                      }}
                    >
                      <HiraginoKakuText
                        normal
                        style={[styles.bodyText, styles.dropdownText]}
                      >
                        {selectedDropDownValue || " "}
                      </HiraginoKakuText>

                      <View style={styles.dropdownIconContainer}>
                        <Entypo
                          name={showDropdown ? "chevron-up" : "chevron-down"}
                          size={22}
                          color={colors.activeCarouselColor}
                          style={styles.dropdownIcon}
                        />
                      </View>
                    </TouchableOpacity>

                    {showDropdown && (
                      <View style={styles.dropdown}>
                        {options.map((option, index) => (
                          <TouchableOpacity
                            key={index}
                            onPress={() => handleDropDownSelectOption(option)}
                          >
                            <HiraginoKakuText
                              normal
                              style={[styles.optionText, styles.bodyText]}
                            >
                              {option}
                            </HiraginoKakuText>
                          </TouchableOpacity>
                        ))}
                      </View>
                    )}
                  </View>
                </View>

                {/* <View style={styles.errorContainer}>
                                   {relationDropDownErr && <HiraginoKakuText normal style={styles.errorText}>代表者との関係性を選択してください</HiraginoKakuText>}
                            </View> */}
              </View>

              <View style={styles.saveBtnContainer}>
                <TouchableOpacity
                  style={[styles.saveBtn]}
                  onPress={() => {
                    handleButtonPress("create");
                    handleCreateButtonPress();
                  }}
                >
                  <HiraginoKakuText
                    style={[styles.saveBtnText, styles.LabelLargeBold]}
                  >
                    保存する
                  </HiraginoKakuText>
                </TouchableOpacity>
              </View>
            </View>
          </View>
        </ScrollView>
        {isSaveModalVisible && (
          <ReceptionInfoSaveDialog
            onUnsaveButtonPress={handleNotSaveButton}
            onSaveButtonPress={handleSaveButton}
            onCancelButtonPress={handleCancelButton}
          />
        )}
      </SafeAreaView>
    </TouchableWithoutFeedback>
  );
};

const RadioButton = (props: any) => {
  const outerBorderColor = props.selected ? "#346DF4" : "#B8BCC7";

  return (
    <View
      style={[
        {
          height: 24,
          width: 24,
          borderRadius: 12,
          borderWidth: 2,
          borderColor: outerBorderColor,
          alignItems: "center",
          justifyContent: "center",
        },
        props.style,
      ]}
    >
      {props.selected ? (
        <View
          style={{
            height: 10,
            width: 10,
            borderRadius: 6,
            backgroundColor: "#346DF4",
          }}
        />
      ) : null}
    </View>
  );
};
