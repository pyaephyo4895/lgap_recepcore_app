import React, { useState } from "react";
import { View } from "react-native";
import ModalComponent from "../../components/basics/ModalComponent";
type ReceptionSavelDialogProps = {
  onUnsaveButtonPress?: () => void;
  onSaveButtonPress?: () => void;
  onCancelButtonPress?: () => void;
};
export const ReceptionInfoSaveDialog = (props: ReceptionSavelDialogProps) => {
  const [isModalVisible, setModalVisible] = useState(true);

  return (
    <View>
      {isModalVisible && (
        <ModalComponent
          text="内容を保存しますか？"
          firstButtonText="保存しない"
          secondButtonText="保存する"
          leftButtonText="キャンセル"
          leftButtonVisible={true}
          onFirstButtonPress={props.onUnsaveButtonPress}
          onSecondButtonPress={props.onSaveButtonPress}
          onLeftButtonPress={props.onCancelButtonPress}
          toggleModal={props.onCancelButtonPress}
          leftButtonType="ButtonMGray"
          firstButtonType="ButtonMSecondary"
          secondButtonWidth={104}
          secondBtnTextWidth={64}
        ></ModalComponent>
      )}
    </View>
  );
};
