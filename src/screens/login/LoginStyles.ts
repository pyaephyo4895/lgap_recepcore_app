import { StyleSheet, Dimensions } from "react-native";
import { colors } from "../../styles/color";
import {
  LabelLargeBold,
  LabelLargeRegular,
  ButtonSmallBold,
  HeadingXSmallBold,
  BodyTextMedium,
} from "../../styles/typography";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";

export const styles = StyleSheet.create({
  mainContainer: {
    width: "100%",
    height: "100%",
  },
  bodyContainer: {
    alignItems: "center",
    paddingHorizontal: wp("3.35%"),
    paddingTop: hp("4.8%"),
    paddingBottom: hp("14.4%"),
    gap: 16,
  },
  loginText: {
    alignItems: "center",
    height: hp("4.3%"),
    fontSize: HeadingXSmallBold.size,
    color: colors.textColor,
  },
  infoBox: {
    width: wp("47%"),
    maxHeight: hp("45.55%"),
    borderRadius: 4,
    backgroundColor: colors.secondary,
    paddingHorizontal: wp("3.35%"),
    paddingVertical: hp("4.8%"),
    borderColor: colors.gray,
    borderWidth: 1,
    gap: 48,
  },
  inputContainer: {
    width: wp("40.3%"),
    gap: 24,
  },
  labelInputSetBox: {
    width: wp("40.3%"),
    gap: 8,
  },
  label: {
    width: wp("40.3%"),
    height: hp("2.9%"),
    fontSize: LabelLargeBold.size,
    lineHeight: LabelLargeBold.lineHeight,
    color: colors.greyTextColor,
    gap: 8,
  },
  passwordBox: {
    flexDirection: "row",
    alignItems: "center",
    position: "relative",
  },
  input: {
    flex: 1,
    borderColor: colors.borderColor,
    borderWidth: 1,
    width: wp("40.3%"),
    height: hp("5.3%"),
    borderRadius: 6,
    paddingHorizontal: wp("0.85%"),
    paddingVertical: hp("1.1%"),
    backgroundColor: colors.secondary,
    fontSize: LabelLargeRegular.size,
    lineHeight: 21,
    gap: 8,
  },

  eyeIconContainer: {
    position: "absolute",
    right: 11,
  },
  eyeIcon: {
    color: colors.activeCarouselColor,
  },
  buttonLogin: {
    width: wp("40.3%"),
    height: hp("5.3%"),
    gap: 8,
    fontSize: ButtonSmallBold.size,
    lineHeight: ButtonSmallBold.lineHeight,
    borderRadius: 4,
  },
  messageContainer: {
    width: wp("40.3%"),
    height: hp("2.9%"),
  },
  errorMessage: {
    fontSize: BodyTextMedium.size,
    lineHeight: 23.8,
    color: colors.danger,
  },
});

export default styles;
