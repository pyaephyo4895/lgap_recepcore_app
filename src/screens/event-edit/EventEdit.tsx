import React, { useState, useRef } from "react";
import {
  View,
  Text,
  TouchableOpacity,
  SafeAreaView,
  StatusBar,
  TextInput,
  ScrollView,
  Pressable,
  KeyboardAvoidingView,
  Platform,
  Keyboard,
  TouchableWithoutFeedback,
} from "react-native";
import styles from "./EventEditStyles";
import { MaterialIcons, Entypo } from "@expo/vector-icons";
import { HiraginoKakuText } from "../../components/StyledText";
import { Header } from "../../components/basics/header";
import { colors } from "../../styles/color";
import { CustomButton } from "../../components/basics/Button";
import { NavigationProp, useRoute } from "@react-navigation/native";
import { EventSaveDialog } from "../event-save-dialog/EventSaveDialog";
import { CustomCalendar } from "../../components/basics/Calendar";
import { format } from "date-fns";

type Props = {
  navigation: NavigationProp<any, any>;
};
export const EventEdit = ({ navigation }: Props) => {
  const route = useRoute();
  const { userId, eventId, pgType, editPgType } = route.params as {
    userId: string;
    eventId: number;
    pgType: string;
    editPgType: string;
  };
  const [inputBoxes, setInputBoxes] = useState([
    { value: "会場A" },
    { value: "会場B" },
  ]);
  const [selectedButton, setSelectedButton] = useState<string | null>(
    "preparation"
  );
  const [isStartDateCalendarVisible, setStartDateCalendarVisible] = useState(false);
  const [isEndDateCalendarVisible, setEndDateCalendarVisible] = useState(false);
  const [startDate, setStartDate] = useState("2024-04-01");
  const [endDate, setEndDate] = useState("2024-04-01");
  const startDateInputRef = useRef(null);
  const endDateInputRef = useRef(null);
  const startDateRef = useRef(null);
  const endDateRef = useRef(null);

  const addInputBox = (event : any) => {
    setInputBoxes([...inputBoxes, { value: "" }]);
    closeCalendar(event);
  };

  const removeInputBox = (removedIndex: number) => {
    setInputBoxes(inputBoxes.filter((box, index) => index !== removedIndex));
    setStartDateCalendarVisible(false);
    setEndDateCalendarVisible(false);
  };

  const handleButtonPress = (buttonName: string) => {
    if (buttonName === selectedButton) {
      if (selectedButton === "create") {
        navigation.navigate("EventList", {
          userId: userId,
        });
      }

      setSelectedButton(null);
    } else {
      setSelectedButton(buttonName);
    }
    setStartDateCalendarVisible(false);
    setEndDateCalendarVisible(false);
  };
  const handleButtonPressMain = () => {
    if (selectedButton == "create") {
      if (editPgType === "EventDetail") {
        navigation.navigate("EventDetail", {
          userId: userId,
          eventId: eventId,
          pgType: pgType,
        });
      } else {
        navigation.navigate("EventList", { userId: userId, eventId: 0 });
      }
    }
    if (selectedButton == "preparation") {
      setIsSaveModalVisible(true);
    } else if (selectedButton == "acceptance") {
      setIsSaveModalVisible(true);
    } else if (selectedButton == "close") {
      setIsSaveModalVisible(true);
    } else if (selectedButton == "archive") {
      setIsSaveModalVisible(true);
    }
    setStartDateCalendarVisible(false);
    setEndDateCalendarVisible(false);
  };

  // Event Save Dialog
  const [isSaveModalVisible, setIsSaveModalVisible] = useState(false);
  const handleCloseEventEdit = () => {
    setStartDateCalendarVisible(false);
    setEndDateCalendarVisible(false);
    setIsSaveModalVisible(true);
  };
  const handleCancelButton = () => {
    setIsSaveModalVisible(false);
    navigation.navigate("EventEdit", {
      userId: userId,
      eventId: eventId,
      pgType: pgType,
      editPgType: editPgType
    });
  };
  const handleNotSaveButton = () => {
    setIsSaveModalVisible(false);
    if (editPgType === "EventDetail") {
      navigation.navigate("EventDetail", {
        userId: userId,
        eventId: eventId,
        pgType: pgType,
      });
    } else {
      navigation.navigate("EventList", {
        userId: userId,
      });
    }
  };
  const handleSaveButton = () => {
    setIsSaveModalVisible(false);
    if (editPgType === "EventDetail") {
      navigation.navigate("EventDetail", {
        userId: userId,
        eventId: eventId,
        pgType: pgType,
      });
    } else {
      navigation.navigate("EventList", {
        userId: userId,
        eventId: eventId,
      });
    }
  };

  const handleStartDateCalendarPress = (event: any) => {
    (startDateInputRef.current as any).focus();
    setStartDateCalendarVisible(!isStartDateCalendarVisible);
    setEndDateCalendarVisible(false);
  };

  const handleEndDateCalendarPress = (event: any) => {
    (endDateInputRef.current as any).focus();
    setEndDateCalendarVisible(!isEndDateCalendarVisible);
    setStartDateCalendarVisible(false);
  };

  const handleStartDateSelect = (date: any) => {
    setStartDate(date);
    setStartDateCalendarVisible(false);
  };

  const handleEndDateSelect = (date: any) => {
    setEndDate(date);
    setEndDateCalendarVisible(false);
  };

  const closeCalendar = (event: any) => {
    if (event.nativeEvent.target != startDateInputRef.current && event.nativeEvent.target != startDateRef) {
      if (isStartDateCalendarVisible) {
        setStartDateCalendarVisible(false);
      }
    }
    if (event.nativeEvent.target != endDateInputRef.current && event.nativeEvent.target != endDate) {
      if (isEndDateCalendarVisible) {
        setEndDateCalendarVisible(false);
      }
    }
  };

  return (
    <KeyboardAvoidingView
      behavior={Platform.OS === "ios" ? "padding" : "height"}
    >
      <SafeAreaView style={styles.mainContainer}>
        <StatusBar barStyle="dark-content" />
        <Header middleTitleName="イベント編集" buttonName="">
          <CustomButton
            text="閉じる"
            type="ButtonSDefault"
            icon={<Entypo name="cross" size={24} color={colors.primary} />}
            iconPosition="front"
            onPress={handleCloseEventEdit}
          />
        </Header>
        <ScrollView style={styles.scrollViewContent}>
          <TouchableWithoutFeedback onPress={closeCalendar}>
            <View style={styles.container}>
              <View style={styles.bodyContainer}>
                <View style={styles.eventTitleContainer}>
                  <View style={styles.eventHeadingLblContainer}>
                    <HiraginoKakuText
                      style={[styles.bodyText, styles.LabelLargeBold]}
                    >
                      イベント名
                    </HiraginoKakuText>
                    <View style={styles.statusLabelContainer}>
                      <View
                        style={[styles.statusBox, styles.eventTitleStatusBox]}
                      >
                        <HiraginoKakuText
                          style={[
                            styles.statusLabelText,
                            styles.eventRedStatusLblText,
                          ]}
                        >
                          必須
                        </HiraginoKakuText>
                      </View>
                    </View>
                  </View>
                  <View>
                    <TextInput
                      style={styles.eventInputBox}
                      defaultValue="出茂マラソン大会2024"
                      onFocus={closeCalendar}
                    />
                  </View>
                </View>
                <View style={styles.eventTimeContainer}>
                  <View style={styles.eventTimeLblGpContainer}>
                    <View style={styles.eventHeadingLblContainer}>
                      <HiraginoKakuText
                        style={[styles.bodyText, styles.LabelLargeBold]}
                      >
                        イベント期間
                      </HiraginoKakuText>
                      <View style={styles.statusLabelContainer}>
                        <View style={styles.statusBox}>
                          <HiraginoKakuText style={styles.statusLabelText}>
                            任意
                          </HiraginoKakuText>
                        </View>
                      </View>
                    </View>
                    <HiraginoKakuText normal style={styles.eventLabel}>
                      開始日と終了日を入力してください
                    </HiraginoKakuText>
                  </View>
                  <View style={styles.dateTimeSelectContainer}>
                    <View style={styles.DateContainer}>
                      <HiraginoKakuText
                        style={[styles.bodyText, styles.LabelLargeBold]}
                      >
                        開始日
                      </HiraginoKakuText>
                      <View style={styles.datePickerBox}>
                        <TextInput
                          ref={startDateInputRef}
                          style={[styles.bodyText, styles.dateInput]}
                          placeholder="日付を選択"
                          placeholderTextColor={colors.placeholderTextColor}
                          value={endDate != "" ? format(new Date(startDate), "yyyy/MM/dd") : startDate}
                          onPressIn={handleStartDateCalendarPress}
                          onPointerDown={handleStartDateCalendarPress}
                          showSoftInputOnFocus={false}
                          onTouchStart={() => Keyboard.dismiss()}
                          editable={false}
                        />
                        <Pressable style={styles.calendarIconContainer}
                          ref={startDateRef}
                          onPress={handleStartDateCalendarPress}>
                          <MaterialIcons
                            name="calendar-today"
                            size={22}
                            color={colors.activeCarouselColor}
                            style={styles.calendarIcon}
                          />
                        </Pressable>
                        {isStartDateCalendarVisible && (
                          <CustomCalendar selectedDate={startDate} onDateSelect={handleStartDateSelect} />
                        )}
                      </View>
                    </View>
                    <View style={styles.waveDash}>
                      <Text style={styles.LabelLargeBold}> ~ </Text>
                    </View>
                    <View style={styles.DateContainer}>
                      <HiraginoKakuText
                        style={[styles.bodyText, styles.LabelLargeBold]}
                      >
                        終了日
                      </HiraginoKakuText>
                      <View style={styles.datePickerBox}>
                        <TextInput
                          ref={endDateInputRef}
                          style={[styles.bodyText, styles.dateInput]}
                          placeholder="日付を選択"
                          placeholderTextColor={colors.placeholderTextColor}
                          value={endDate != "" ? format(new Date(endDate), "yyyy/MM/dd") : endDate}
                          onPressIn={handleEndDateCalendarPress}
                          onPointerDown={handleEndDateCalendarPress}
                          showSoftInputOnFocus={false}
                          onTouchStart={() => Keyboard.dismiss()}
                          editable={false}
                        />
                        <Pressable style={styles.calendarIconContainer}
                          ref={endDateRef}
                          onPress={handleEndDateCalendarPress}>
                          <MaterialIcons
                            name="calendar-today"
                            size={22}
                            color={colors.activeCarouselColor}
                            style={styles.calendarIcon}
                          />
                        </Pressable>
                        {isEndDateCalendarVisible && (
                          <CustomCalendar selectedDate={endDate} onDateSelect={handleEndDateSelect} />
                        )}
                      </View>
                    </View>
                  </View>
                </View>
                <View style={styles.eventVenueContainer}>
                  <View style={styles.eventHeadingLblContainer}>
                    <HiraginoKakuText
                      style={[styles.bodyText, styles.LabelLargeBold]}
                    >
                      会場
                    </HiraginoKakuText>
                    <View style={styles.statusLabelContainer}>
                      <View style={styles.statusBox}>
                        <HiraginoKakuText style={styles.statusLabelText}>
                          任意
                        </HiraginoKakuText>
                      </View>
                    </View>
                  </View>
                  <View style={styles.venueInputBoxesContainer}>
                    {inputBoxes.map((box, index) => (
                      <View key={index} style={styles.venueInputBox}>
                        <TextInput
                          style={[styles.eventInputBox, styles.venueInput]}
                          defaultValue={box.value}
                          onFocus={closeCalendar}
                        />
                        {inputBoxes.length > 1 && (
                          <TouchableOpacity
                            onPress={() => removeInputBox(index)}
                            style={styles.venueCrossIconContainer}
                          >
                            <MaterialIcons
                              name="close"
                              size={22}
                              color="#515867"
                            />
                          </TouchableOpacity>
                        )}
                      </View>
                    ))}
                    <TouchableOpacity
                      style={styles.venueBtnContainer}
                      onPress={addInputBox}
                    >
                      <MaterialIcons name="add" size={22} color="#515867" />
                      <HiraginoKakuText
                        style={[
                          styles.bodyText,
                          styles.LabelLargeBold,
                          styles.venueBtnText,
                        ]}
                      >
                        会場を追加
                      </HiraginoKakuText>
                    </TouchableOpacity>
                  </View>
                </View>
                <View style={styles.horizontalLine} />
                <View style={styles.eventReceptionContainer}>
                  <View style={styles.eventHeadingLblContainer}>
                    <HiraginoKakuText
                      style={[styles.bodyText, styles.LabelLargeBold]}
                    >
                      イベントの受付状況
                    </HiraginoKakuText>
                    <View style={styles.statusLabelContainer}>
                      <View
                        style={[styles.statusBox, styles.eventTitleStatusBox]}
                      >
                        <HiraginoKakuText
                          style={[
                            styles.statusLabelText,
                            styles.eventRedStatusLblText,
                          ]}
                        >
                          必須
                        </HiraginoKakuText>
                      </View>
                    </View>
                  </View>
                  <HiraginoKakuText normal style={styles.eventLabel}>
                    受付可能にすると受付アプリに表示されます
                  </HiraginoKakuText>
                  <View style={styles.eventReceptionBtnContainer}>
                    <TouchableOpacity
                      style={[
                        styles.receptionBtn,
                        selectedButton === "preparation" && styles.btnSelected,
                      ]}
                      onPress={() => handleButtonPress("preparation")}
                    >
                      <HiraginoKakuText
                        style={[styles.bodyText, styles.LabelLargeBold]}
                      >
                        準備中
                      </HiraginoKakuText>
                    </TouchableOpacity>
                    <TouchableOpacity
                      style={[
                        styles.receptionBtn,
                        styles.eventMedianBtn,
                        selectedButton === "acceptance" && styles.btnSelected,
                      ]}
                      onPress={() => handleButtonPress("acceptance")}
                    >
                      <HiraginoKakuText
                        style={[styles.bodyText, styles.LabelLargeBold]}
                      >
                        受付可能
                      </HiraginoKakuText>
                    </TouchableOpacity>
                    <TouchableOpacity
                      style={[
                        styles.receptionBtn,
                        styles.eventMedianBtn,
                        selectedButton === "close" && styles.btnSelected,
                      ]}
                      onPress={() => handleButtonPress("close")}
                    >
                      <HiraginoKakuText
                        style={[styles.bodyText, styles.LabelLargeBold]}
                      >
                        受付終了
                      </HiraginoKakuText>
                    </TouchableOpacity>
                    <TouchableOpacity
                      style={[
                        styles.receptionBtn,
                        styles.eventLargeBtn,
                        selectedButton === "archive" && styles.btnSelected,
                      ]}
                      onPress={() => handleButtonPress("archive")}
                    >
                      <HiraginoKakuText
                        style={[styles.bodyText, styles.LabelLargeBold]}
                      >
                        アーカイブ
                      </HiraginoKakuText>
                    </TouchableOpacity>
                  </View>
                </View>
              </View>
              <View style={styles.createBtnContainer}>
                <TouchableOpacity
                  style={[
                    styles.createBtn,
                    selectedButton === "create" && styles.btnSelected,
                  ]}
                  onPress={() => handleButtonPressMain()}
                >
                  <HiraginoKakuText
                    style={[styles.createBtnText, styles.LabelLargeBold]}
                  >
                    保存する
                  </HiraginoKakuText>
                </TouchableOpacity>
              </View>
            </View>
          </TouchableWithoutFeedback>
        </ScrollView>
        {isSaveModalVisible && (
          <EventSaveDialog
            onUnsaveButtonPress={handleNotSaveButton}
            onSaveButtonPress={handleSaveButton}
            onCancelButtonPress={handleCancelButton}
          />
        )}
      </SafeAreaView>
    </KeyboardAvoidingView>
  );
};
